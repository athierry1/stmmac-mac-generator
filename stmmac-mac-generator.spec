Name:		stmmac-mac-generator
Version:	1.0
Release:	2%{?dist}
Summary:	Correct random stmmac 1GB MAC address
BuildArch:	noarch
License:	GPLv2
URL:		https://gitlab.com/CentOS/automotive/rpms/stmmac-mac-generator/
Source0:	stmmac-mac-generator.service
Source1:    stmmac-mac-generator.sh

%description
Generates a systemd link file with a MAC address based on the board
serial number for the 1GB stmmac NICs that come up with a fully random
MAC address.

%prep

%install
mkdir -p "%{buildroot}/lib/systemd/system/" "%{buildroot}/usr/bin/"
install %{SOURCE0} "%{buildroot}/lib/systemd/system/stmmac-mac-generator.service"
install %{SOURCE1} "%{buildroot}/usr/bin/"

%post
/usr/bin/stmmac-mac-generator.sh

%postun
rm -f /run/systemd/network/10-stmmac-1gb-23000000.link
rm -f /run/systemd/network/10-stmmac-1gb-20000.link

%files
/lib/systemd/system/stmmac-mac-generator.service
/usr/bin/stmmac-mac-generator.sh

%changelog
* Thu Apr 06 2023 Adrien Thierry <athierry@redhat.com> 1.0-2
- Use machine-id as fallback if serial number is not present

* Thu Mar 16 2023 Adrien Thierry <athierry@redhat.com> 1.0
- First commit!
